import { Fleet } from "../../fleet";
import { CoordinateHelper } from "../../helpers";

export interface Rules {
    makeFleet(): Fleet;
    width: number;
    height: number;
    coordinateHelper: CoordinateHelper;
}