import { Rules } from "./rules";
import { Fleet, FleetBuilder } from "../../fleet";
import { Deployment, RandomDeployment } from "../../deployment";
import { CoordinateHelper, RandomHelper } from "../../helpers";

export class GuestlineRules implements Rules {
    private readonly _width = 10;
    private readonly _height = 10;
    private readonly _coordinateHelper: CoordinateHelper;
    private readonly _randomHelper: RandomHelper;
    private readonly _deploymentStrategy: Deployment;
    private readonly _fleetBuilder: FleetBuilder;

    constructor() {
        this._coordinateHelper = new CoordinateHelper(this._width, this._height);
        this._randomHelper = new RandomHelper();
        this._deploymentStrategy = new RandomDeployment(this._width, this._height, this._coordinateHelper, this._randomHelper);
        this._fleetBuilder = new FleetBuilder(this._width, this._height, this._deploymentStrategy);
    }

    makeFleet(): Fleet {
        return this._fleetBuilder
            .reset()
            .addShip(5)
            .addShip(4)
            .addShip(4)
            .build();
    }

    get width(): number {
        return this._width;
    }

    get height(): number {
        return this._height;
    }

    get coordinateHelper(): CoordinateHelper {
        return this._coordinateHelper;
    }
}