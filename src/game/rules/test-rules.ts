import { Rules } from "./rules";
import { CoordinateHelper } from "../../helpers";
import { Fleet, Ship } from "../../fleet";

export class TestRules implements Rules {
    height: number = 10;
    width: number = 10;
    coordinateHelper: CoordinateHelper = new CoordinateHelper(this.width, this.height);

    makeFleet(): Fleet {
        return new Fleet(new Map([[new Ship(["A1", "A2"]), ["A1", "A2"]]]));
    }
}
