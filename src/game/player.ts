import { Game } from "./game";
import { Input, Output } from "../io";
import { GameCommand } from "./game-command";
import { Command } from "./command";

export class Player {
    private input: Input;
    private output: Output;

    constructor(input: Input, output: Output) {
        this.input = input;
        this.output = output;
    }

    async play(game: Game) {
        this.outputBaseScreen(game);
        this.output.writeln("");
        this.output.writeln("");

        while (game.inProgress()) {
            const command = await this.input.readCommand();
            const result = game.exec(command);

            this.outputBaseScreen(game);
            this.outputResult(result, command.payload);
        }

        this.output.writeln(game.exec(new Command(GameCommand.OUTRO)));
    }

    gameOver() {
        this.input.close();
    }

    private outputBaseScreen(game: Game) {
        this.output.cls();
        this.output.writeln(game.exec(new Command(GameCommand.INTRO)));
        this.output.writeln(game.exec(new Command(GameCommand.BOARD)));
        this.output.writeln("");
    }

    private outputResult(result: string, payload?: string) {
        this.output.writeln(payload !== undefined ? payload : "");
        this.output.writeln(result);
    }
}