import { Rules } from "./rules";
import { Fleet } from "../fleet";
import { Coordinate, Gameplay } from "../types";
import { AttackResult } from "./attack-result";
import { GameIO } from "./game-io";
import { GameCommand } from "./game-command";
import { Command } from "./command";

export class Game {
    private finished: boolean = false;
    private rules: Rules;
    private fleet?: Fleet;
    private gameplay?: Gameplay;
    private gameIO: GameIO;

    constructor(rules: Rules) {
        this.rules = rules;
        this.gameIO = new GameIO(this.rules.width, this.rules.height, this.rules.coordinateHelper);
        this.exec(new Command(GameCommand.RESET));
    }

    inProgress(): boolean {
        return !this.finished && !this.fleet!.sunken();
    }

    exec(command: Command): string {
        switch (command.gameCommand) {
            case GameCommand.SHOT: {
                if (command.payload === undefined) {
                    return this.gameIO.error();
                }
                const result = this.fleet!.attack(command.payload);
                this.gameplay!.set(command.payload, result);
                return String(result);
            }

            case GameCommand.BOARD: {
                return this.gameIO.board(this.gameplay!);
            }

            case GameCommand.INTRO: {
                return this.gameIO.intro();
            }

            case GameCommand.OUTRO: {
                return this.gameIO.outro(this.fleet!.sunken());
            }

            case GameCommand.EXIT: {
                this.finished = true;
                return "";
            }

            case GameCommand.RESET: {
                this.finished = false;
                this.fleet = this.rules.makeFleet();
                this.gameplay = new Map<Coordinate, AttackResult>();
                return "";
            }

            default: {
                return this.gameIO.error();
            }
        }
    }
}