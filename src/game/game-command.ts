export enum GameCommand {
    EXIT, BOARD, INTRO, OUTRO, SHOT, RESET, NOOP
}