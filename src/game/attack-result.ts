export enum AttackResult {
    Hit = "Hit",
    Miss = "Miss",
    Sink = "Sink",
}