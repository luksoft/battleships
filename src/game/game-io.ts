import { AttackResult } from "./attack-result";
import { CoordinateHelper } from "../helpers";
import { Gameplay } from "../types";

export class GameIO {
    private readonly width: number;
    private readonly height: number;
    private readonly rowHeaderWidth: number = 3;
    private readonly coordinateHelper: CoordinateHelper;

    constructor(width: number, height: number, coordinateHelper: CoordinateHelper) {
        this.width = width;
        this.height = height;
        this.coordinateHelper = coordinateHelper;
    }

    intro(): string {
        return [
            "",
            "Welcome to the battleships game!",
            "Start shooting (a1, b5 etc.) or enter 'q', 'e' or 'exit' to exit the game\n"].join("\n");
    }

    outro(fleetSunken: boolean): string {
        const msg = [];
        if (fleetSunken) {
            msg.push("You've won!!!");
        }
        msg.push("See ya");
        return msg.join("\n");
    }

    board(gameplay: Gameplay): string {
        const board: Array<string> = [], separator = " ";

        board.push(this.boardHeader().join(separator));

        for (let row = 0, height = this.height; row < height; row++) {
            board.push(this.boardRow(row, gameplay).join(separator));
        }

        return board.join("\n");
    }

    error(): string {
        return "Unknown command";
    }

    private boardHeader(): Array<string> {
        const boardHeader = [" ".padStart(this.rowHeaderWidth, " ")];
        for (let column = 0, width = this.width; column < width; column++) {
            boardHeader.push(this.coordinateHelper.fromColumn(column));
        }
        return boardHeader;
    }

    private boardRow(row: number, gameplay: Gameplay): Array<string> {
        const boardRow = [this.coordinateHelper.fromRow(row).padStart(this.rowHeaderWidth, " ")];

        for (let column = 0, width = this.width; column < width; column++) {
            const coordinate = this.coordinateHelper.fromColumnAndRow(column, row);
            let result: string;

            switch (gameplay.get(coordinate)) {
                case AttackResult.Miss: {
                    result = "*";
                    break;
                }

                case AttackResult.Hit:
                case AttackResult.Sink: {
                    result = "X";
                    break;
                }

                default: {
                    result = " ";
                }
            }
            boardRow.push(result);
        }

        return boardRow;
    }
}