import { GameIO } from "./game-io";
import { CoordinateHelper } from "../helpers";
import { AttackResult } from "./attack-result";

const width = 10, height = 10;
const coordinateHelper = new CoordinateHelper(width, height);

it("should render board", () => {
    const gameIO = new GameIO(width, height, coordinateHelper);
    const gameplay = new Map([
        ["A1", AttackResult.Hit], ["A2", AttackResult.Sink],
        ["D6", AttackResult.Hit], ["E6", AttackResult.Sink], ["F6", AttackResult.Hit], ["G6", AttackResult.Hit],
        ["J1", AttackResult.Miss], ["E8", AttackResult.Miss], ["A10", AttackResult.Miss], ["J10", AttackResult.Miss],
    ]);

    const board = [
        "    A B C D E F G H I J",
        "  1 X                 *",
        "  2 X                  ",
        "  3                    ",
        "  4                    ",
        "  5                    ",
        "  6       X X X X      ",
        "  7                    ",
        "  8         *          ",
        "  9                    ",
        " 10 *                 *",
    ].join("\n");

    expect(gameIO.board(gameplay)).toEqual(board);
});