import { GameCommand } from "./game-command";

export class Command {
    public gameCommand: GameCommand;
    public payload?: string;

    constructor(gameCommand: GameCommand, payload?: string) {
        this.gameCommand = gameCommand;
        this.payload = payload;
    }
}