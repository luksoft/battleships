import { Game } from "./game";
import { TestRules } from "./rules/test-rules";
import { Player } from "./player";
import { BruteForceInput } from "../io";
import { NullOutput } from "../io/null-output";

const rules = new TestRules();
const game = new Game(rules);

const output = new NullOutput();
const input = new BruteForceInput(output, rules.width, rules.height);
const player = new Player(input, output);


it("should play the game to the end", async () => {
    expect(game.inProgress()).toBeTruthy();
    await player.play(game);
    expect(game.inProgress()).toBeFalsy();
});
