import { Game } from "./game";
import { GameCommand } from "./game-command";
import { AttackResult } from "./attack-result";
import { TestRules } from "./rules/test-rules";
import { Command } from "./command";

it("should be in progress after creation", () => {
    const game = new Game(new TestRules());

    expect(game.inProgress()).toBeTruthy();
});

it("should finish after fleet sunken", () => {
    const game = new Game(new TestRules());

    game.exec(new Command(GameCommand.SHOT, "A1"));
    game.exec(new Command(GameCommand.SHOT, "A2"));

    expect(game.inProgress()).toBeFalsy();
});

it("should finish after exit command", () => {
    const game = new Game(new TestRules());

    game.exec(new Command(GameCommand.EXIT));

    expect(game.inProgress()).toBeFalsy();
});

describe("should be in progress after reset command", () => {
    it("should be in progress after exit", () => {
        const game = new Game(new TestRules());

        game.exec(new Command(GameCommand.EXIT));
        game.exec(new Command(GameCommand.RESET));

        expect(game.inProgress()).toBeTruthy();
    });

    it("should be in progress after fleet sunken", () => {
        const game = new Game(new TestRules());

        game.exec(new Command(GameCommand.SHOT, "A1"));
        game.exec(new Command(GameCommand.SHOT, "A2"));
        game.exec(new Command(GameCommand.RESET));

        expect(game.inProgress()).toBeTruthy();
    });
});

it("should return miss result", () => {
    const game = new Game(new TestRules());

    expect(game.exec(new Command(GameCommand.SHOT, "C1"))).toBe(AttackResult.Miss);
});

it("should return hit result", () => {
    const game = new Game(new TestRules());

    expect(game.exec(new Command(GameCommand.SHOT, "A1"))).toBe(AttackResult.Hit);
});

it("should return sink result", () => {
    const game = new Game(new TestRules());

    game.exec(new Command(GameCommand.SHOT, "A1"));
    expect(game.exec(new Command(GameCommand.SHOT, "A2"))).toBe(AttackResult.Sink);
});

