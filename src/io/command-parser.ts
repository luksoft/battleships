import { Command, GameCommand } from "../game";
import { CoordinateHelper } from "../helpers";
import { Coordinate } from "../types";

export class CommandParser {
    private coordinateHelper: CoordinateHelper;
    private commands: Map<string, GameCommand> = new Map([
        ["EXIT", GameCommand.EXIT],
        ["E", GameCommand.EXIT],
        ["Q", GameCommand.EXIT],
    ]);

    constructor(coordinateHelper: CoordinateHelper) {
        this.coordinateHelper = coordinateHelper;
    }

    parse(input?: string): Command {
        if (input === undefined) {
            return new Command(GameCommand.NOOP);
        }

        input = input.trim().toUpperCase();

        if (this.commands.has(input)) {
            return new Command(this.commands.get(input)!);
        }

        try {
            this.coordinateHelper.validate(<Coordinate>input);
            return new Command(GameCommand.SHOT, input);
        } catch (e) {
            return new Command(GameCommand.NOOP);
        }
    }
}