import { Output } from "./output";

export class ConsoleLogOutput implements Output {
    writeln(message: string): void {
        console.log(message);
    }

    cls(): void {
        console.clear();
    }
}