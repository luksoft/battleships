import { Command } from "../game";

export interface Input {
    close(): void;

    readCommand(): Command | Promise<Command>;
}