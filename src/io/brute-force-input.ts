import { Input } from "./input";
import { CoordinateHelper } from "../helpers";
import { Output } from "./output";
import { Command, GameCommand } from "../game";

export class BruteForceInput implements Input {
    private readonly output: Output;
    private readonly width: number;
    private readonly height: number;
    private readonly coordinateHelper: CoordinateHelper;
    private iterator: IterableIterator<string>;

    constructor(output: Output, width: number, height: number, coordinateHelper?: CoordinateHelper) {
        this.output = output;
        this.width = width;
        this.height = height;
        this.coordinateHelper = coordinateHelper ? coordinateHelper : new CoordinateHelper(width, height);
        this.iterator = this.coordinates();
    }

    close(): void {
        this.iterator!.return!();
    }

    readCommand(): Command {
        const coordinate = this.iterator!.next().value;
        this.output.writeln(`> ${coordinate}`);
        return new Command(GameCommand.SHOT, coordinate);
    }

    private* coordinates(): IterableIterator<string> {
        for (let column = 0; column < this.width; column++) {
            for (let row = 0; row < this.height; row++) {
                yield this.coordinateHelper.fromColumnAndRow(column, row);
            }
        }
    }
}