import { CommandParser } from "./command-parser";
import { Command } from "../game";
import { Input } from "./input";
import * as readline from "readline";
import { Interface } from "readline";
import { Output } from "./output";

export class ConsoleInput implements Input {
    private cli: Interface;
    private output: Output;
    private commandParser: CommandParser;

    constructor(output: Output, commandParser: CommandParser) {
        this.output = output;
        this.commandParser = commandParser;

        this.cli = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            prompt: "> "
        });
        this.setupCli();
    }

    close(): void {
        this.cli.close();
    }

    readCommand(): Promise<Command> {
        return new Promise((resolve, reject) => {
            this.cli.question("> ", answer => {
                resolve(this.commandParser.parse(answer));
            });
        });
    }

    private setupCli() {
        this.cli.on("close", () => {
            this.output.writeln("Exiting...");
        });
        this.cli.on("SIGINT", () => {
            this.cli.close();
        });
    }
}