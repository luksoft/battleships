export interface Output {
    writeln(message: string): void;
    cls(): void;
}