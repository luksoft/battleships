import { CommandParser } from "./command-parser";
import { CoordinateHelper } from "../helpers";
import { Command, GameCommand } from "../game";

const coordinateHelper = new CoordinateHelper();

it("should return empty command for undefined input", () => {
    const parser = new CommandParser(coordinateHelper);

    expect(parser.parse()).toEqual(new Command(GameCommand.NOOP));
});

it.each([
    [GameCommand[GameCommand.EXIT], "EXIT", new Command(GameCommand.EXIT)],
    [GameCommand[GameCommand.EXIT], "exit", new Command(GameCommand.EXIT)],
    [GameCommand[GameCommand.EXIT], "E", new Command(GameCommand.EXIT)],
    [GameCommand[GameCommand.EXIT], "e", new Command(GameCommand.EXIT)],
    [GameCommand[GameCommand.EXIT], "Q", new Command(GameCommand.EXIT)],
    [GameCommand[GameCommand.EXIT], "q", new Command(GameCommand.EXIT)],
])("should return game command '%s' for input: '%s'", (gameCommandName, input, command) => {
    const parser = new CommandParser(coordinateHelper);

    expect(parser.parse(input)).toEqual(command);
});

it.each([["A1"], ["A10"], ["J1"], ["J10"], ["F5"]])
("should return 'SHOT' command for input: '%s'", (input) => {
    const parser = new CommandParser(coordinateHelper);

    expect(parser.parse(input)).toEqual(new Command(GameCommand.SHOT, input));
});

it.each([[".1"], ["Z10"], ["A."], ["C20"], ["C-1"], ["foo"], ["C0"]])
("should return empty command for unrecognized input: '%s'", (input) => {
    const parser = new CommandParser(coordinateHelper);

    expect(parser.parse(input)).toEqual(new Command(GameCommand.NOOP));
});
