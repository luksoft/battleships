import { Output } from "./output";

export class NullOutput implements Output {
    writeln(message: string): void {
    }

    cls(): void {
    }
}