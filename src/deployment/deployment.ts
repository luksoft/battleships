import { Coordinates, ShipSize } from "../types";

export interface Deployment {
    reset(): void;
    placeShip(size: ShipSize): Coordinates;
}