import { Coordinates, ShipSize } from "../types";
import { Deployment } from "./deployment";
import { DeploymentError } from "./deployment-error";

export class FixedDeployment implements Deployment {
    private coordinates?: Array<Coordinates>;

    constructor() {
        this.reset();
    }

    reset(): void {
        this.coordinates = [
            ["A1", "A2", "A3", "A4", "A5"],
            ["C3", "D3", "E3", "F3"],
            ["G7", "G8", "G9", "G10"],
        ];
    }

    placeShip(size: ShipSize): Coordinates {
        const coordinateIdx = this.coordinates!.findIndex(coordinate => coordinate.length == size);

        if (coordinateIdx < 0) {
            throw new DeploymentError("Failed to place a ship");
        }

        return this.coordinates!.splice(coordinateIdx, 1)[0];
    }
}