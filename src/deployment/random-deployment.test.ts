import { CoordinateHelper, RandomHelper } from "../helpers";
import { RandomDeployment } from "./random-deployment";
import { DeploymentError } from "./deployment-error";

class FakeRandomHelper extends RandomHelper {
    private readonly booleanSequence: Array<boolean>;
    private readonly numberInRangeSequence: Array<number>;

    constructor(booleanSequence: Array<boolean>, numberInRangeSequence: Array<number>) {
        super();
        this.booleanSequence = booleanSequence;
        this.numberInRangeSequence = numberInRangeSequence;
    }

    randomBoolean(): boolean {
        return this.booleanSequence.shift()!;
    }

    randomNumberInRange(min: number, max: number): number {
        return this.numberInRangeSequence.shift()!;
    }
}

const width = 10, height = 10,
    horizontal = true, vertical = false,
    coordinateHelper = new CoordinateHelper(width, height);

it("shouldn't build any ship bigger than board dimensions", () => {
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, new FakeRandomHelper([], []));

    function placeShip() {
        randomDeployment.placeShip(11);
    }

    expect(placeShip).toThrowError(DeploymentError);
});

it("should place ship horizontally", () => {
    const startCoordinates = [1, 1];
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, new FakeRandomHelper([horizontal], [...startCoordinates]));

    expect(randomDeployment.placeShip(3)).toEqual(["B2", "C2", "D2"]);
});

it("should place ship vertically", () => {
    const startCoordinates = [2, 2];
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, new FakeRandomHelper([vertical], [...startCoordinates]));

    expect(randomDeployment.placeShip(3)).toEqual(["C3", "C4", "C5"]);
});

it("should find new horizontal place if there is a collision", () => {
    const startCoordinates = [2, 2], collidingCoordinates = [2, 2], notCollidingCoordinates = [3, 3];
    const randomHelper = new FakeRandomHelper(
        [vertical, vertical, horizontal],
        [...startCoordinates, ...collidingCoordinates, ...notCollidingCoordinates]
    );
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, randomHelper);

    randomDeployment.placeShip(3);

    expect(randomDeployment.placeShip(3)).toEqual(["D4", "E4", "F4"]);
});

it("should find new vertical place if there is a collision", () => {
    const startCoordinates = [2, 2], collidingCoordinates = [2, 2], notCollidingCoordinates = [3, 3];
    const randomHelper = new FakeRandomHelper(
        [horizontal, horizontal, vertical],
        [...startCoordinates, ...collidingCoordinates, ...notCollidingCoordinates]
    );
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, randomHelper);

    randomDeployment.placeShip(3);

    expect(randomDeployment.placeShip(3)).toEqual(["D4", "D5", "D6"]);
});

it("should throw error when could not find place for ship", () => {
    const startCoordinates = [2, 2], collidingCoordinates = [2, 2], notCollidingCoordinates = [3, 3];
    const randomHelper = new FakeRandomHelper(
        [horizontal, horizontal, vertical],
        [...startCoordinates, ...collidingCoordinates, ...notCollidingCoordinates]
    );
    const randomDeployment = new RandomDeployment(width, height, coordinateHelper, randomHelper, 1);

    randomDeployment.placeShip(3);

    function placeShip() {
        randomDeployment.placeShip(3);
    }

    expect(placeShip).toThrowError(DeploymentError);
});
