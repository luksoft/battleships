import { Coordinate, Coordinates, ShipSize } from "../types";
import { CoordinateHelper, RandomHelper } from "../helpers";
import { Deployment } from "./deployment";
import { DeploymentError } from "./deployment-error";

export class RandomDeployment implements Deployment {
    private readonly width: number;
    private readonly height: number;
    private readonly coordinateHelper: CoordinateHelper;
    private readonly randomHelper: RandomHelper;
    private readonly maxDeploymentAttempts: number;
    private board?: Map<Coordinate, Boolean>;

    constructor(width: number,
                height: number,
                coordinateHelper: CoordinateHelper,
                randomHelper: RandomHelper,
                maxDeploymentAttempts: number = 10,
    ) {
        this.width = width;
        this.height = height;
        this.coordinateHelper = coordinateHelper;
        this.randomHelper = randomHelper;
        this.maxDeploymentAttempts = maxDeploymentAttempts;
        this.reset();
    }

    reset(): void {
        this.board = new Map<Coordinate, Boolean>();
    }

    placeShip(size: ShipSize): Coordinates {
        if (size > this.width && size > this.height) {
            throw new DeploymentError("Ship is too big");
        }

        let coordinates: Coordinates | undefined,
            attempts: number = this.maxDeploymentAttempts;

        while (attempts--) {
            coordinates = this.findPlace(size);

            if (coordinates) {
                return coordinates;
            }
        }

        throw new DeploymentError("Failed to place a ship");
    }

    private findPlace(size: ShipSize): Coordinates | undefined {
        return this.deployHorizontally() ? this.findHorizontalPlace(size) : this.findVerticalPlace(size);
    }

    private findHorizontalPlace(size: ShipSize): Coordinates | undefined {
        if (size > this.width) {
            return undefined;
        }

        const coordinates: Coordinates = [];
        const startColumn = this.randomPosition(0, this.width - size);
        const endColumn = startColumn + size;
        const row = this.randomPosition(0, this.height);

        if (this.willCollideHorizontally(startColumn, endColumn, row)) {
            return undefined;
        }

        for (let column = startColumn; column < endColumn; column++) {
            const coordinate = this.coordinateHelper.fromColumnAndRow(column, row);
            this.board!.set(coordinate, true);
            coordinates.push(coordinate);
        }

        return coordinates;
    }

    private willCollideHorizontally(startColumn: number, endColumn: number, row: number): boolean {
        for (let x = startColumn; x < endColumn; x++) {
            if (this.board!.has(this.coordinateHelper.fromColumnAndRow(x, row))) {
                return true;
            }
        }

        return false;
    }

    private findVerticalPlace(size: ShipSize): Coordinates | undefined {
        if (size > this.height) {
            return undefined;
        }

        const coordinates: Coordinates = [];
        const startRow = this.randomPosition(0, this.height - size);
        const endRow = startRow + size;
        const column = this.randomPosition(0, this.width);

        if (this.willCollideVertically(startRow, endRow, column)) {
            return undefined;
        }

        for (let row = startRow; row < endRow; row++) {
            const coordinate = this.coordinateHelper.fromColumnAndRow(column, row);
            this.board!.set(coordinate, true);
            coordinates.push(coordinate);
        }

        return coordinates;
    }

    private willCollideVertically(startRow: number, endRow: number, column: number): boolean {
        for (let y = startRow; y < endRow; y++) {
            if (this.board!.has(this.coordinateHelper.fromColumnAndRow(column, y))) {
                return true;
            }
        }

        return false;
    }

    private deployHorizontally(): boolean {
        return this.randomHelper.randomBoolean();
    }

    private randomPosition(min: number, max: number): number {
        return this.randomHelper.randomNumberInRange(min, max);
    }
}