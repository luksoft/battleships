import { FixedDeployment } from "./fixed-deployment";
import { DeploymentError } from "./deployment-error";

describe("should return all ships", () => {
    it("should return 1 battleship", () => {
        const fixedDeployment = new FixedDeployment();

        expect(fixedDeployment.placeShip(5)).toEqual(["A1", "A2", "A3", "A4", "A5"]);
    });

    it("should return 2 destroyers", () => {
        const fixedDeployment = new FixedDeployment();

        expect(fixedDeployment.placeShip(4)).toEqual(["C3", "D3", "E3", "F3"]);
        expect(fixedDeployment.placeShip(4)).toEqual(["G7", "G8", "G9", "G10"]);
    });
});

describe("should throw error when there are no ships of given size to return", () => {
    it("should throw error when trying to place 2nd battleship", () => {
        const fixedDeployment = new FixedDeployment();

        function placeShip() {
            fixedDeployment.placeShip(5);
        }

        expect(fixedDeployment.placeShip(5)).toEqual(["A1", "A2", "A3", "A4", "A5"]);
        expect(placeShip).toThrowError(DeploymentError);
    });

    it("should throw error when trying to place 3nd destroyer", () => {
        const fixedDeployment = new FixedDeployment();

        function placeShip() {
            fixedDeployment.placeShip(4);
        }

        expect(fixedDeployment.placeShip(4)).toEqual(["C3", "D3", "E3", "F3"]);
        expect(fixedDeployment.placeShip(4)).toEqual(["G7", "G8", "G9", "G10"]);
        expect(placeShip).toThrowError(DeploymentError);
    });

    it("should throw error when trying to place not supported ship", () => {
        const fixedDeployment = new FixedDeployment();

        function placeShip() {
            fixedDeployment.placeShip(3);
        }

        expect(placeShip).toThrowError(DeploymentError);
    });
});
