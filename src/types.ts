import { AttackResult, GameCommand } from "./game";

export type Coordinate = string;
export type Coordinates = Array<Coordinate>;
export type ShipSize = number;
export type Gameplay = Map<Coordinate, AttackResult>;
