import { Coordinate } from "../types";
import { CoordinateError } from "./coordinate-error";

enum ColumnNames {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, W, V, X, Y, Z
}

export class CoordinateHelper {
    private readonly width: number;
    private readonly height: number;

    constructor(width: number = 10, height: number = 10) {
        this.width = width;
        this.height = height;

        if (ColumnNames[this.width - 1] === undefined) {
            throw new CoordinateError(`Maximum coordinate width exceeded`);
        }
    }

    fromColumn(column: number): string {
        this.validateColumnAndRow(column, 0);
        return `${ColumnNames[column]}`;
    }

    fromRow(row: number): string {
        this.validateColumnAndRow(0, row);
        return String(Math.round(row) + 1);
    }

    fromColumnAndRow(column: number, row: number): Coordinate {
        this.validateColumnAndRow(column, row);
        return `${ColumnNames[column]}${row + 1}`;
    }

    toColumnAndRow(coordinate: Coordinate): [number, number] {
        const [column, row] = this.parseCoordinate(coordinate);
        this.validateColumnAndRow(column, row);
        return [column, row];
    }

    validate(coordinate: Coordinate) {
        this.validateColumnAndRow(...this.parseCoordinate(coordinate));
    }

    private validateColumnAndRow(column: number, row: number) {
        if (isNaN(column) || column < 0 || column >= this.width) {
            throw new CoordinateError(`Invalid coordinate column`);
        }

        if (isNaN(row) || row < 0 || row >= this.height) {
            throw new CoordinateError(`Invalid coordinate row`);
        }
    }

    private parseCoordinate(coordinate: Coordinate): [number, number] {
        return [
            (<any>ColumnNames)[coordinate[0]],
            parseInt(coordinate.slice(1)) - 1
        ];
    }
}