import { RandomHelper } from "./random-helper";

it("will return boolean value", () => {
    const helper = new RandomHelper();

    expect(typeof helper.randomBoolean()).toBe("boolean");
});

it("will return number value", () => {
    const helper = new RandomHelper();

    expect(typeof helper.randomNumberInRange(0, 1)).toBe("number");
});

it("will return number greater than or equal lower bound", () => {
    const helper = new RandomHelper();
    const min = 0;
    const max = 2;

    expect(helper.randomNumberInRange(min, max)).toBeGreaterThanOrEqual(min);
});

it("will return number less than upper bound", () => {
    const helper = new RandomHelper();
    const min = 0;
    const max = 2;

    expect(helper.randomNumberInRange(min, max)).toBeLessThan(max);
});