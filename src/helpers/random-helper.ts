export class RandomHelper {
    randomBoolean(): boolean {
        return Math.round(Math.random()) > 0;
    }

    randomNumberInRange(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
}