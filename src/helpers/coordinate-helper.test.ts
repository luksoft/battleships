import { CoordinateHelper } from "./coordinate-helper";
import { CoordinateError } from "./coordinate-error";

const width = 10, height = 10, maxWidth = 26;

it("should throw exception if width is to big", () => {
    function newHelper() {
        new CoordinateHelper(maxWidth + 1, height);
    }

    expect(newHelper).toThrow(CoordinateError);
});

it("shouldn't throw exception if width is within range", () => {
    function newHelper() {
        new CoordinateHelper(maxWidth, height);
    }

    expect(newHelper).not.toThrow(CoordinateError);
});

describe("fromColumn", () => {
    it.each([["A", 0], ["B", 1], ["I", 8], ["J", 9]])
    ("should return %s for column %i", (columnName, columnNumber) => {
        const helper = new CoordinateHelper(width, height);

        expect(helper.fromColumn(columnNumber)).toBe(columnName);
    });
});

describe("fromRow", () => {
    it.each([["1", 0], ["2", 1], ["9", 8], ["10", 9]])
    ("should return %s for row %i", (rowName, rowNumber) => {
        const helper = new CoordinateHelper(width, height);

        expect(helper.fromRow(rowNumber)).toBe(rowName);
    });
});

describe("fromColumnAndRow", () => {
it.each([["A1", 0, 0], ["A10", 0, 9], ["J1", 9, 0], ["J10", 9, 9]])
("should return %s for column %i and row %i", (coordinate, columnNumber, rowNumber) => {
    const helper = new CoordinateHelper(width, height);

    expect(helper.fromColumnAndRow(columnNumber, rowNumber)).toBe(coordinate);
});
});

describe("toColumnAndRow", () => {
it.each([[0, 0, "A1"], [0, 9, "A10"], [9, 0, "J1"], [9, 9, "J10"]])
("should return column %i and row %i for coordinate %s", (columnNumber, rowNumber, coordinate) => {
    const helper = new CoordinateHelper(width, height);

    expect(helper.toColumnAndRow(coordinate)).toEqual([columnNumber, rowNumber]);
});
});

describe("validate", () => {
    it.each([[".1"], ["Z10"], ["A."], ["C20"], ["C-1"], ["foo"], ["C0"], ["a1"]])
    ("should throw exception for invalid coordinate: '%s'", (coordinate) => {
        const helper = new CoordinateHelper(width, height);

        function validate() {
            helper.validate(coordinate);
        }

        expect(validate).toThrow(CoordinateError);
    });

    it.each([["A1"], ["A10"], ["J1"], ["J10"]])
    ("shouldn't throw exception for valid coordinate: '%s'", (coordinate) => {
        const helper = new CoordinateHelper(width, height);

        function validate() {
            helper.validate(coordinate);
        }

        expect(validate).not.toThrow(CoordinateError);
    });
});
