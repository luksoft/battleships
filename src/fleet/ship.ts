import { ShipSegment } from "./ship-segment";
import { Coordinate, Coordinates } from "../types";

export class Ship {
    private readonly segmentCoordinates = new Map<Coordinate, ShipSegment>();

    constructor(coordinates: Coordinates) {
        coordinates.forEach(coordinate => {
            this.segmentCoordinates.set(coordinate, new ShipSegment());
        });
    }

    hit(coordinate: Coordinate) {
        if (this.segmentCoordinates.has(coordinate)) {
            this.segmentCoordinates.get(coordinate)!.hit();
        }
    }

    sunken(): boolean {
        for (const segment of this.segmentCoordinates.values()) {
            if (!segment.isDestroyed()) {
                return false;
            }
        }

        return true;
    }
}