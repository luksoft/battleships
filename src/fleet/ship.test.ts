import { Ship } from "./ship";

it("should be sunken after hitting all of the coordinates", () => {
    const ship = new Ship(["a", "b", "c"]);

    ship.hit("a");
    ship.hit("b");
    ship.hit("c");

    expect(ship.sunken()).toBeTruthy();
});

it("shouldn't be sunken after hitting some of the coordinates", () => {
    const ship = new Ship(["a", "b", "c"]);

    ship.hit("a");
    ship.hit("b");

    expect(ship.sunken()).toBeFalsy();
});

it("shouldn't be sunken after hitting at the same coordinate multiple times", () => {
    const ship = new Ship(["a", "b", "c"]);

    ship.hit("a");
    ship.hit("a");
    ship.hit("a");

    expect(ship.sunken()).toBeFalsy();
});
