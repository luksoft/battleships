import { AttackResult } from "../game";
import { Coordinate, Coordinates } from "../types";
import { Ship } from "./ship";

export class Fleet {
    private readonly ships: Array<Ship> = [];
    private readonly shipCoordinates: Map<Coordinate, Ship> = new Map<string, Ship>();

    constructor(ships: Map<Ship, Coordinates>) {
        ships.forEach(((coordinates, ship) => {
            this.ships.push(ship);
            coordinates.forEach(coordinate => {
                this.shipCoordinates.set(coordinate, ship);
            });
        }));
    }

    attack(coordinate: Coordinate): AttackResult {
        if (!this.shipCoordinates.has(coordinate)) {
            return AttackResult.Miss;
        }

        const ship = this.shipCoordinates.get(coordinate)!;
        ship.hit(coordinate);

        return ship.sunken() ? AttackResult.Sink : AttackResult.Hit;
    }

    sunken(): boolean {
        return this.ships.filter(ship => ship.sunken()).length == this.size();
    }

    size(): number {
        return this.ships.length;
    }
}
