export class ShipSegment {
    private destroyed: boolean = false;

    hit() {
        this.destroyed = true;
    }

    isDestroyed(): boolean {
        return this.destroyed;
    }
}
