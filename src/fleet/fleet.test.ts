import { Ship } from "./ship";
import { Fleet } from "./fleet";
import { AttackResult } from "../game";

let fleet: Fleet;

beforeEach(() => {
    const ship1Coordinates = ["a1", "b1", "c1"];
    const ship1 = new Ship(ship1Coordinates);
    const ship2Coordinates = ["d2", "d3"];
    const ship2 = new Ship(ship2Coordinates);
    const ship3Coordinates = ["e10"];
    const ship3 = new Ship(ship3Coordinates);
    const ships = new Map([[ship1, ship1Coordinates], [ship2, ship2Coordinates], [ship3, ship3Coordinates]]);
    fleet = new Fleet(ships);
});

it("should have correct size", () => {
    expect(fleet.size()).toEqual(3);
});

it("fleet shouldn't be sunken after creation", () => {
    expect(fleet.sunken()).toBeFalsy();
});

it("should miss after attack at not existing coordinates", () => {
    expect(fleet.attack("foo")).toEqual(AttackResult.Miss);
    expect(fleet.attack("bar")).toEqual(AttackResult.Miss);
});

it("should hit after attack at existing coordinates", () => {
    expect(fleet.attack("b1")).toEqual(AttackResult.Hit);
    expect(fleet.attack("c1")).toEqual(AttackResult.Hit);
    expect(fleet.attack("d2")).toEqual(AttackResult.Hit);
});

it("should sunk ship after attack at all ship's coordinates", () => {
    fleet.attack("a1");
    fleet.attack("c1");
    fleet.attack("d3");

    expect(fleet.attack("b1")).toEqual(AttackResult.Sink);
    expect(fleet.attack("d2")).toEqual(AttackResult.Sink);
    expect(fleet.attack("e10")).toEqual(AttackResult.Sink);
});

it("fleet should be sunken after sinking all ships", () => {
    fleet.attack("a1");
    fleet.attack("c1");
    fleet.attack("b1");
    fleet.attack("d3");
    fleet.attack("d2");
    fleet.attack("e10");

    expect(fleet.sunken()).toBeTruthy();
});
