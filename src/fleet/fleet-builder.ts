import { Coordinates, ShipSize } from "../types";
import { Ship } from "./ship";
import { Fleet } from "./fleet";
import { Deployment } from "../deployment";

export class FleetBuilder {
    private width?: number;
    private height?: number;
    private shipSizes?: Array<ShipSize>;
    private deploymentStrategy?: Deployment;

    constructor(width: number, height: number, deploymentStrategy: Deployment) {
        this.deploymentStrategy = deploymentStrategy;
        this.width = width;
        this.height = height;
        this.reset();
    }

    reset() {
        this.shipSizes = [];
        return this;
    }

    addShip(size: ShipSize): FleetBuilder {
        this.shipSizes!.push(Math.trunc(size));
        return this;
    }

    build(): Fleet {
        this.deploymentStrategy!.reset();

        return new Fleet(new Map(
            this.shipSizes!.map(// for every ship size
                size => this.makeShip(// make new Ship object
                    this.deploymentStrategy!.placeShip(size) // based on board coordinates
                )
            )
        ));
    }

    private makeShip(coordinates: Coordinates): [Ship, Coordinates] {
        return [new Ship(coordinates), coordinates];
    }
}