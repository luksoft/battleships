import { ShipSegment } from "./ship-segment";

it("should be not destroyed after creation", () => {
    const shipSegment = new ShipSegment();

    expect(shipSegment.isDestroyed()).toBeFalsy();
});

it("should be destroyed after hit", () => {
    const shipSegment = new ShipSegment();

    shipSegment.hit();

    expect(shipSegment.isDestroyed()).toBeTruthy();
});
