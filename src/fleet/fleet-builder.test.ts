import { FleetBuilder } from "./fleet-builder";
import { FixedDeployment } from "../deployment";

it("should build the fleet from all added ships", () => {
    const width = 7, height = 10;
    const builder: FleetBuilder = new FleetBuilder(width, height, new FixedDeployment());

    builder.addShip(5).addShip(4).addShip(4);

    expect(builder.build().size()).toEqual(3);
});

it("should build new fleet every time", () => {
    const width = 7, height = 10;
    const builder: FleetBuilder = new FleetBuilder(width, height, new FixedDeployment());

    builder.addShip(5).addShip(4).addShip(4);
    const fleet1 = builder.build(), fleet2 = builder.build();

    expect(fleet1).not.toBe(fleet2);
});