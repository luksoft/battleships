import { Game, GuestlineRules, Player } from "./game";
import { CommandParser, ConsoleInput, ConsoleLogOutput } from "./io";

try {
    const rules = new GuestlineRules();
    const game = new Game(rules);

    const commandParser = new CommandParser(rules.coordinateHelper);
    const output = new ConsoleLogOutput();
    const input = new ConsoleInput(output, commandParser);
    const player = new Player(input, output);

    player
        .play(game)
        .then(() => {
            player.gameOver();
        });

} catch (e) {
    console.log(`General runtime error: ${e.message}\nExiting`);
}
