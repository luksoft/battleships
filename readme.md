#BattleShips

## Requirements

* NodeJS v10.15
* npm v6.7

## Build

```
> npm install
> npm run build
```

## Test

```
> npm test
```

## Run

```
> npm start
```

